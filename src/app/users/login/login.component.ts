import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { SocialAuthService } from "angularx-social-login";
import { GoogleLoginProvider } from "angularx-social-login";
import { addUser } from 'src/app/user.action';
import { addTask } from 'src/app/user.action';
import { resetStore} from 'src/app/user.action';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent   {

    constructor(private authService: SocialAuthService, private router :Router , private store: Store) {}
    user="";
    logInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((res) => {
      console.log(res);
      this.user=res.email;
      
      let admin=JSON.parse(localStorage.getItem("admin"));
        
      let isAdmin1=false;
       
      let isUser=false;
      admin.forEach(element => {
               
     if(element.name==this.user)
     {
       console.log(element+"  sdihfjdsf")
     isAdmin1=true;
  
     }
    });
    
      let user=JSON.parse(localStorage.getItem("user"));
      user.forEach(element => {
        this.addUser(element.name, element.isAdmin); 
        
       if(element.name==this.user&&element.isAdmin==false)
        {
          isUser=true;

        }
      });
      if(isAdmin1)
      {
        this.router.navigate(
          [
            '/userlist'
          ],
          { state: { email: this.user } }
        )
      }
       
       else if(isUser){
          let tasks=JSON.parse(localStorage.getItem(this.user))
          if(tasks!=null)
          {
            tasks.forEach(element => {
                this.onAdd(element.task, element.status);
            });
          }
       this.router.navigate(
        [
          '/home1'
        ],
        { state: { email: this.user } }
      )

        }
        else{
          this.router.navigate(
            [
              '/home'
            ],
            { state: { email: this.user } }
          )
          
        }
     })
    .catch(err=> console.log(err))
     
  }
  onAdd(task: string, status: boolean) {
    this.store.dispatch(addTask({ task: task, status: status }));
  }

  logOut(): void {
    this.onresetStore();
    this.authService.signOut();
  }
  addUser(user, isAdmin) {
    this.onAddUser(user, isAdmin);
  }
  onAddUser(name: string, isAdmin: boolean) {
    this.store.dispatch(addUser({ name: this.user, isAdmin: isAdmin }));
  }
  onresetStore()
  {
    this.store.dispatch(resetStore());
  }  
}
