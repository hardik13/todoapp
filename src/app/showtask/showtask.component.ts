import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-showtask',
  templateUrl: './showtask.component.html',
  styleUrls: ['./showtask.component.css']
})
export class ShowtaskComponent implements OnInit {
name=""

  constructor(private router: Router,) {
    const name=this.router.getCurrentNavigation().extras.state.name;
    this.name=name;
  }
  tasks=[]
  ngOnInit(): void {
      this.tasks=JSON.parse(localStorage.getItem(this.name));

  }

    
}
