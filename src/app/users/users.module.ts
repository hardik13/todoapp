import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';

import {FormsModule } from '@angular/forms';
import { Home1Component } from './home1/home1.component';

@NgModule({
  declarations: [LoginComponent, SignupComponent, Home1Component ],
  imports: [
    CommonModule,
    FormsModule 
], exports:[
    LoginComponent
    ,SignupComponent,
   
  ]
  
})
export class UsersModule { }
