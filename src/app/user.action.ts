import { createAction, props } from "@ngrx/store";

export const addTask = createAction(
  "[User Page] Add Task",
  props<{ task: string; status: boolean }>()
);

export const doneTask = createAction(
  "[User Page] Done Task",
  props<{ task: string }>()
);
export const addUser = createAction(
    "[Admin Page] Add User",
    props<{ name: string , isAdmin : boolean  }>()
  );
export const resetStore=createAction(
    "[User Reset Page] Reset Task",
     
) ; 
