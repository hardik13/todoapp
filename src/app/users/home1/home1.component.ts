import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { SocialAuthService } from 'angularx-social-login';
import { addTask, doneTask } from 'src/app/user.action';
import { resetStore} from 'src/app/user.action';

@Component({
  selector: 'app-home1',
  templateUrl: './home1.component.html',
  styleUrls: ['./home1.component.css']
})
export class Home1Component  {
 
//   constructor(private authService: SocialAuthService, private router :Router ) {}


//   addTodo(newTo)
//   {
//     var newTod={
//       name:newTo,
//       status:false,
   
//     };
//     this.todoo.push(newTod);
//   }
//deleteTodo(todoop)
//   {
//     this.todoo=this.todoo.filter(t => t.name!==todoop.name);
//   }
   
//   logOut(): void {
//     this.authService.signOut();this.router.navigate(["/"]);}

// }
user;

constructor(
  private router: Router,
  private authService: SocialAuthService,
  private store: Store

  )
{
 
   const email = this.router.getCurrentNavigation().extras.state.email;
   this.user=email;

} 


tasks =[];

ngOnInit() {
  this.store
    .pipe(select((state: any) => state.user.tasks))
    .subscribe((response) => {
    this.tasks=response,
    console.log("Response: ", response);
    });
}

add(task) {
  let taskList = JSON.parse(localStorage.getItem(this.user));
  if (taskList == null && task != "") {
    localStorage.setItem(
      this.user,
      JSON.stringify([{ task: task, status: false }])
    );
  } else if (taskList != null && task != "") {
    taskList.push({ task: task, status: false });
    localStorage.setItem(this.user, JSON.stringify(taskList));
  }
  if (task != "") this.onAdd(task, false);
}
done(task) {
  this.onDone(task);
  
}




logOut(): void {
  this.onresetStore();
    
  this.authService.signOut();
  
  this.router.navigate(["/"]);
}
onAdd(task: string, status: boolean) {
  this.store.dispatch(addTask({ task: task, status: status }));
}
onresetStore()
{
  this.store.dispatch(resetStore());
}  
onDone(task: string) {
  let taskList = JSON.parse(localStorage.getItem(this.user));
  taskList = taskList.map((ele) => {
    if (ele.task == task) {
      ele.status = true;
    }
    return ele;
  });
  localStorage.setItem(this.user, JSON.stringify(taskList));
  this.store.dispatch(doneTask({ task: task}));
}

  } 
    
