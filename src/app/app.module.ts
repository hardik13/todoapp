import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ReactiveFormsModule} from                '@angular/forms';
import { UserlistComponent } from './userlist/userlist.component';
import {UsersModule} from './users/users.module';
import {FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import {
  SocialLoginModule,
  SocialAuthServiceConfig,
} from "angularx-social-login";
import { GoogleLoginProvider } from "angularx-social-login";
import { HomeComponent } from './home/home.component';
import { StoreModule } from '@ngrx/store';
import * as fromUser from "./user.reducer";
import { ShowtaskComponent } from './showtask/showtask.component';
@NgModule({
  declarations: [
    AppComponent,
    UserlistComponent,
    HomeComponent,
    ShowtaskComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    SocialLoginModule,
    UsersModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule.forRoot(),
    StoreModule.forRoot({ user: fromUser.reducer }),    
    

  ],
  
  providers: [
    {
      provide: "SocialAuthServiceConfig",
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              "980682752251-80821qkuj2n7trt190e359mk1si2sa8e.apps.googleusercontent.com"
            ),
          },
        ],
      } as SocialAuthServiceConfig,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
