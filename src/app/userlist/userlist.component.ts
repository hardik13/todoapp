import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { SocialAuthService } from 'angularx-social-login';
import { resetStore} from 'src/app/user.action';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

  constructor( private router: Router,
    private authService: SocialAuthService,
    private store: Store
   ) { }
names=[]
  userslist=[]
    ngOnInit(): void {
    
    this.store
    .pipe(select((state: any) => state.user.names))
    .subscribe((response) => {
    this.names=response,
      
    console.log("Response: ", response);
     
      });
      let user=JSON.parse(localStorage.getItem("user"));
      user.forEach(element => {
        // this.addUser(element.name, element.isAdmin); 
       this.userslist.push(element.name); 
       
      });
      
}
showtask(item)
{
  this.router.navigate(["/showtask"], {
    state:{name:item} 
  })
}
n;
arr: [];
logOut(): void {
  this.onresetStore();
    
  this.authService.signOut();
  
  this.router.navigate(["/"]);
}

checkLen()
{
  this.n=localStorage.length;
}
per;
localStore(key)
{
  this.per= JSON.parse(localStorage.getItem(key))
}
onresetStore()
{
  this.store.dispatch(resetStore());
}  
}
