import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocialAuthService } from "angularx-social-login";
import { GoogleLoginProvider } from "angularx-social-login";
import { select, Store } from '@ngrx/store';
import { addUser } from 'src/app/user.action';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {

  constructor(private authService: SocialAuthService, private router: Router, private store: Store
  ) {

  }
  names = [];

  userlist = [];
  adminlist = [];
  ngOnInit() {
    this.store
      .pipe(select((state: any) => state.user.names))
      .subscribe((response) => {
        this.names = response,
          console.log("Response: ", response);
      });
  }
  user = "";
  isAdmin = false;
  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((res) => {
      this.user = res.email;
    }) .catch(err=> console.log(err))
  }

  signOut(): void {
    this.authService.signOut();
  }

  signup(form) {


    if (form.admin == "admin") {
      this.isAdmin = true;
      this.router.navigate(
        [
          '/userlist'
        ])

      // this.addUser(this.user, this.isAdmin);        
      let adminlist = JSON.parse(localStorage.getItem("admin"));

      console.log(adminlist)
      if (adminlist == null) {
        localStorage.setItem("admin", JSON.stringify([{ name: this.user, isAdmin: this.isAdmin }]));
      }
      else {
        adminlist.push({ name: this.user, isAdmin: this.isAdmin });

        localStorage.setItem("admin", JSON.stringify(adminlist));
        adminlist.forEach(element => {
          this.addUser(element.name, element.isAdmin);
        });
      }

    }

    else {

      // localStorage.setItem("user",JSON.stringify(this.user));
      let userlist = JSON.parse(localStorage.getItem("user"));
      console.log(userlist)
      if (userlist == null) {
        localStorage.setItem("user", JSON.stringify([{ name: this.user, isAdmin: this.isAdmin }]));

      }
      else {
        userlist.push({ name: this.user, isAdmin: this.isAdmin });

        localStorage.setItem("user", JSON.stringify(userlist));
        userlist.forEach(element => {
          this.addUser(element.name, element.isAdmin);
        });
      }
      this.router.navigate(
        [
          '/home1'
        ],
        { state: { email: this.user } }
      )
    }

    console.log(this.user, this.isAdmin);
  }

  addUser(user, isAdmin) {
    this.onAddUser(this.user, this.isAdmin);
  }
  onAddUser(name: string, isAdmin: boolean) {
    this.store.dispatch(addUser({ name: this.user, isAdmin: this.isAdmin }));
  }
}
