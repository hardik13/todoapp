import { Action, createReducer, on } from "@ngrx/store";
import * as UserActions from "./user.action";

export const initialState = {
    tasks: [],
    names: []
};
const userReducer = createReducer(
    initialState,
    on(UserActions.addTask, (state, { task, status }) => {
        console.log(task, status);
        
        return {
            ...state,
            tasks: [ { task: task, status: status }, ...state.tasks],
        };
    }),
    on(UserActions.addUser, (state, { name, isAdmin }) => {
        console.log(name);
        return {
            ...state,
            names: [...state.names, { name: name, isAdmin: isAdmin }],
        };
    }),
         on(UserActions.doneTask, (state: any, { task }) => {
        console.log(task);
        const newTasks = [...state.tasks];
        newTasks.splice(newTasks.findIndex(taskEl => taskEl.task === task), 1);
       
        return {
            ...state,
            tasks: [...newTasks, {task, status: true}]
        };

    }),
    on(UserActions.resetStore, state => ({tasks:[], names:[] })
        ),


);

export function reducer(state, action: Action) {
    return userReducer(state, action);
}
