import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ShowtaskComponent } from './showtask/showtask.component';
import { UserlistComponent } from './userlist/userlist.component';
import { Home1Component } from './users/home1/home1.component';
import { LoginComponent } from "./users/login/login.component";
import { SignupComponent } from "./users/signup/signup.component";
const routes: Routes = [{ path: "login", component: LoginComponent },
{ path: "signup", component: SignupComponent },
{ path: "home1", component: Home1Component },
{ path: "userlist", component: UserlistComponent },
{path: "showtask", component: ShowtaskComponent },

{ path: "home", component: HomeComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]

}
)
export class AppRoutingModule { }
